
  

# README

Hi all! Thanks for your interest in taking a look at my GitLab project :)

This is what I've been using to deploy and maintain my personal infrastructure to the cloud.

My goal is to make this all automated and not rely on any manual work.

For now, I'm using 2 small Oracle VM's on Oracle Cloud (1GB RAM + 2vCPU's) to deploy Traefik, BitWarden and my personal webpage.

Here you'll find that I'm using simple docker containers, but in the future, I intend to migrate to Kubernetes, helm charts and other cool stuff.
It'll take time, but I'll get there =)

Feel free to use all the code you see here and suggest whatever you feel that makes sense.
 
Thanks,
Pedro
